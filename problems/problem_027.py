# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):

    if len(values) == 0:
        return None

    max = 0
    for num in values:
        if num > max:
            max = num
    return max
